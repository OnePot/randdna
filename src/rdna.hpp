#include <string>

using std::string;
using namespace std;

string randDNA(int seed, string bases, int n) 
{
	string dna = "";
	
	int min= 0; 
	int max= 3;
	
	mt19937 eng(seed); 
	
	uniform_int_distribution<> unifrm (min,max);
	
	for( int k = 0; k < n; k++)
	{
		dna += bases [unifrm(eng)];
		//cout << dna << endl;
	}
	
	return dna;


}
